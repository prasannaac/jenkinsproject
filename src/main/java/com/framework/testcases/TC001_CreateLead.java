package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC001_CreateLead extends ProjectMethods{
	
	@BeforeTest
	public void setData() {
		testCaseName="TC001_CreateLead";
		testDescription="CreateLead in TestLeaf";
		testNodes="Leads";
		author="Prasanna";
		category="Smoke";
		dataSheetName="TC001";
	}
	
	@Test(dataProvider="fetchData")
	public void login(String username, String password,String companyname,String forename, String lastname) {
		new LoginPage()
		.enterUsername(username)
		.enterPassword(password)
		.clickLogin()
		.clickCRMSFA()
		.ClickLeads()
		.clickCreateLead()
		.enterCompanyName(companyname)
		.enterForeName(forename)
		.enterLastName(lastname)
		.clickCreateLead();
	}
	
}
