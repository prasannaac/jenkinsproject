package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class FindLeadPage extends ProjectMethods {
   
	@FindBy(how = How.XPATH, using = "(//input[@name = 'firstName'])[1]") WebElement eleFirstname;
	@FindBy(how = How.XPATH, using ="(//button[text() = 'Find Leads'])[1]") WebElement eleClickFindButton;
	@FindBy(how = How.XPATH, using = "(//a[text() = 'Sridevi'])[1]") WebElement eleClickLeadList;
	public FindLeadPage() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}

	public FindLeadPage searchByFirstName() {
		clearAndType(eleFirstname, "sridevi");
		return this;
	}
	
	public FindLeadPage clickFindLeadsButton() {
		click(eleClickFindButton);
		return this;
	}
	
	public ViewLeadPage clickLeadList() {
		click(eleClickLeadList);
		return new ViewLeadPage();
		
	}
}
